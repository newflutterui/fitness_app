import 'package:fitness_application/Activity.dart';
import 'package:fitness_application/Explore.dart';
import 'package:fitness_application/Workout.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        height: 64,
        width: 400,
        decoration: const BoxDecoration(
          color: Color.fromRGBO(25, 33, 38, 1),
          borderRadius: BorderRadius.all(
            Radius.circular(40),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Home(),
                  ),
                );
              },
              child: Container(
                height: 36,
                width: 98,
                child: Image.asset('assets/images/image40.png'),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Explore(),
                  ),
                );
              },
              child: Container(
                height: 36,
                width: 98,
                child: Image.asset('assets/images/image36.png'),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Activity(),
                  ),
                );
              },
              child: Container(
                height: 36,
                width: 98,
                child: Image.asset('assets/images/image37.png'),
              ),
            ),
            Container(
              height: 36,
              width: 98,
              child: Image.asset('assets/images/image38.png'),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 20),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              child: Text(
                'Good Morning',
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              child: Text(
                'Pramuditya Uzumaki',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
            const SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Container(
                height: 50,
                width: double.infinity,
                decoration: const BoxDecoration(color: Colors.white),
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(4.0),
                      borderSide: const BorderSide(
                        color: Colors.white,
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(4.0),
                      borderSide: const BorderSide(
                        color: Colors.white,
                      ),
                    ),
                    hintText: 'Search',
                    prefixIcon: const Icon(Icons.search),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 20),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              child: Text(
                'Popular Workouts',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
            const SizedBox(height: 20),
            SizedBox(
              height: 174,
              child: ListView.builder(
                itemCount: 5,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const Workout(),
                        ),
                      );
                    },
                    child: Container(
                      margin: const EdgeInsets.only(left: 20),
                      height: 174,
                      width: 280,
                      child: Image.asset('assets/images/image25.png'),
                    ),
                  );
                },
              ),
            ),
            const SizedBox(height: 20),
            const Row(
              children: [
                SizedBox(
                  width: 20,
                ),
                Text(
                  'Today Plan',
                  style: TextStyle(
                      //color: Color.fromRGBO(255, 255, 255, 0.5),
                      fontSize: 18,
                      fontWeight: FontWeight.w700),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              height: 120,
              width: 450,
              child: Image.asset('assets/images/image24.png'),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              height: 120,
              width: 450,
              child: Image.asset('assets/images/image27.png'),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              height: 120,
              width: 450,
              child: Image.asset('assets/images/image21.png'),
            ),
          ],
        ),
      ),
    );
  }
}
