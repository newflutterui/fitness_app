import 'package:fitness_application/Activity.dart';
import 'package:fitness_application/Home.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Explore extends StatefulWidget {
  const Explore({super.key});

  @override
  State<Explore> createState() => _ExploreState();
}

class _ExploreState extends State<Explore> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        height: 64,
        width: 400,
        decoration: const BoxDecoration(
          color: Color.fromRGBO(25, 33, 38, 1),
          borderRadius: BorderRadius.all(
            Radius.circular(40),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Home(),
                  ),
                );
              },
              child: Container(
                height: 36,
                width: 98,
                child: Image.asset('assets/images/image41.png'),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Explore(),
                  ),
                );
              },
              child: Container(
                height: 36,
                width: 98,
                child: Image.asset('assets/images/image39.png'),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Activity(),
                  ),
                );
              },
              child: Container(
                height: 36,
                width: 98,
                child: Image.asset('assets/images/image37.png'),
              ),
            ),
            Container(
              height: 36,
              width: 98,
              child: Image.asset('assets/images/image38.png'),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                const SizedBox(
                  width: 20,
                ),
                Container(
                  height: 180,
                  width: 450,
                  child: Image.asset(
                    'assets/images/image2.png',
                    fit: BoxFit.fill,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            const Row(
              children: [
                SizedBox(
                  width: 20,
                ),
                Text(
                  'Best for you',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                const SizedBox(
                  width: 20,
                ),
                Container(
                  height: 86,
                  width: 194,
                  child: Image.asset(
                    'assets/images/image9.png',
                    fit: BoxFit.fill,
                  ),
                ),
                const SizedBox(
                  width: 20,
                ),
                Container(
                  height: 86,
                  width: 194,
                  child: Image.asset(
                    'assets/images/image11.png',
                    fit: BoxFit.fill,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                const SizedBox(
                  width: 10,
                ),
                Container(
                  height: 86,
                  width: 194,
                  child: Image.asset(
                    'assets/images/image30.png',
                    fit: BoxFit.fill,
                  ),
                ),
                const SizedBox(
                  width: 20,
                ),
                Container(
                  height: 86,
                  width: 194,
                  child: Image.asset(
                    'assets/images/image13.png',
                    fit: BoxFit.fill,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            const Row(
              children: [
                SizedBox(
                  width: 20,
                ),
                Text(
                  'Challenge',
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                const SizedBox(
                  width: 20,
                ),
                Container(
                  height: 110,
                  width: 110,
                  child: Image.asset('assets/images/image16.png'),
                ),
                const SizedBox(
                  width: 10,
                ),
                Container(
                  height: 110,
                  width: 110,
                  child: Image.asset('assets/images/image15.png'),
                ),
                const SizedBox(
                  width: 10,
                ),
                Container(
                  height: 110,
                  width: 110,
                  child: Image.asset('assets/images/image26.png'),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            const Row(
              children: [
                SizedBox(
                  width: 20,
                ),
                Text(
                  'Fast Warmup',
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                const SizedBox(
                  width: 20,
                ),
                Container(
                  height: 86,
                  width: 194,
                  child: Image.asset(
                    'assets/images/image35.png',
                    fit: BoxFit.fill,
                  ),
                ),
                const SizedBox(
                  width: 20,
                ),
                Container(
                  height: 86,
                  width: 194,
                  child: Image.asset(
                    'assets/images/image10.png',
                    fit: BoxFit.fill,
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
