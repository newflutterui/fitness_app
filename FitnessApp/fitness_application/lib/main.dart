import 'package:fitness_application/Activity.dart';
import 'package:fitness_application/Explore.dart';
import 'package:fitness_application/Home.dart';
import 'package:fitness_application/Onboarding.dart';
import 'package:fitness_application/Workout.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Onboarding(),
    );
  }
}
