import 'package:fitness_application/Explore.dart';
import 'package:fitness_application/Home.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Activity extends StatefulWidget {
  const Activity({super.key});

  @override
  State<Activity> createState() => _ActivityState();
}

class _ActivityState extends State<Activity> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        height: 64,
        width: 400,
        decoration: const BoxDecoration(
          color: Color.fromRGBO(25, 33, 38, 1),
          borderRadius: BorderRadius.all(
            Radius.circular(40),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Home(),
                  ),
                );
              },
              child: Container(
                height: 36,
                width: 98,
                child: Image.asset('assets/images/image41.png'),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Explore(),
                  ),
                );
              },
              child: Container(
                height: 36,
                width: 98,
                child: Image.asset('assets/images/image36.png'),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Activity(),
                  ),
                );
              },
              child: Container(
                height: 36,
                width: 98,
                child: Image.asset('assets/images/image32.png'),
              ),
            ),
            Container(
              height: 36,
              width: 98,
              child: Image.asset('assets/images/image38.png'),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            const Row(
              children: [
                SizedBox(
                  width: 20,
                ),
                Text(
                  'July 2022',
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              height: 64,
              width: 450,
              child: Image.asset(
                'assets/images/image31.png',
                fit: BoxFit.fill,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Row(
              children: [
                SizedBox(
                  width: 20,
                ),
                Text(
                  'Today Report',
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ],
            ),
            Container(
              height: 218,
              width: 450,
              child: Row(
                children: [
                  Column(
                    children: [
                      Container(
                        height: 70,
                        width: 112,
                        child: Image.asset('assets/images/image33.png'),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Container(
                        height: 132,
                        width: 112,
                        child: Image.asset('assets/images/image19.png'),
                      ),
                    ],
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Container(
                    height: 218,
                    width: 222,
                    child: Image.asset('assets/images/image29.png'),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              height: 167,
              width: 450,
              child: Row(
                children: [
                  Container(
                    height: 167,
                    width: 200,
                    child: Image.asset('assets/images/image23.png'),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Column(
                    children: [
                      Container(
                        height: 100,
                        width: 135,
                        child: Image.asset('assets/images/image18.png'),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Container(
                        height: 51,
                        width: 135,
                        child: Image.asset('assets/images/image3.png'),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            // const SizedBox(
            //   height: 10,
            // ),
            Container(
              height: 167,
              width: 450,
              child: Row(
                children: [
                  Container(
                    height: 128,
                    width: 178,
                    child: Image.asset('assets/images/image17.png'),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Container(
                    height: 128,
                    width: 156,
                    child: Image.asset('assets/images/image20.png'),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
